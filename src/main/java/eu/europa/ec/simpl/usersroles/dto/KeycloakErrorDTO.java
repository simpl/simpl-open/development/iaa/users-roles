package eu.europa.ec.simpl.usersroles.dto;

import lombok.Data;

@Data
public class KeycloakErrorDTO {
    String errorMessage;
}
