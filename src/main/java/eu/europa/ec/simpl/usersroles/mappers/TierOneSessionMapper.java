package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.common.model.dto.usersroles.TierOneSessionDTO;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface TierOneSessionMapper {

    eu.europa.ec.simpl.api.authenticationprovider.v1.model.TierOneSessionDTO toAuthenticationProviderDTO(
            TierOneSessionDTO session);
}
