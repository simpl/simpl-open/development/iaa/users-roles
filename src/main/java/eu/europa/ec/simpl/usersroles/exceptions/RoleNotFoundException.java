package eu.europa.ec.simpl.usersroles.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.UUID;
import org.springframework.http.HttpStatus;

public class RoleNotFoundException extends StatusException {

    private final UUID roleId;

    public RoleNotFoundException(String name) {
        super(HttpStatus.NOT_FOUND, String.format("Role %s not found", name));
        this.roleId = null;
    }

    public RoleNotFoundException(UUID roleId, String message) {
        super(HttpStatus.NOT_FOUND, message);
        this.roleId = roleId;
    }

    public UUID getRoleId() {
        return roleId;
    }
}
