package eu.europa.ec.simpl.usersroles.configurations.keycloak;

import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "keycloak")
public record KeycloakProperties(
        String url, ClientProperties master, ClientProperties app, RoleMigrationProperties clientToRealmRoleMigration) {

    public record ClientProperties(String realm, String clientId, String user, String password) {}

    public record RoleMigrationProperties(boolean enabled, List<String> clientIds) {}
}
