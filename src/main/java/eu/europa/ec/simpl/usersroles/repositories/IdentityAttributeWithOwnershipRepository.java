package eu.europa.ec.simpl.usersroles.repositories;

import eu.europa.ec.simpl.usersroles.entities.IdentityAttributeWithOwnership;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IdentityAttributeWithOwnershipRepository
        extends JpaRepository<IdentityAttributeWithOwnership, UUID>,
                JpaSpecificationExecutor<IdentityAttributeWithOwnership> {

    @Query("select ida from IdentityAttributeWithOwnership ida join fetch ida.participantTypes")
    List<IdentityAttributeWithOwnership> findAllFetchParticipantTypes();

    List<IdentityAttributeWithOwnership> findAllByCodeIn(List<String> identityAttributesCodes);
}
