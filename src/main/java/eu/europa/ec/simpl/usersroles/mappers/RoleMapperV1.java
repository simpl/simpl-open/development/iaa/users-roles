package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.api.usersroles.v1.model.PageResponseRoleDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.RoleDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.SearchRolesFilterParameterDTO;
import eu.europa.ec.simpl.common.filters.RoleFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import java.util.List;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface RoleMapperV1 {

    RoleDTO toV1(eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO roleDTO);

    RoleFilter toV0(SearchRolesFilterParameterDTO filter);

    PageResponseRoleDTO toV1(PageResponse<eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO> search);

    KeycloakRoleDTO toV0(eu.europa.ec.simpl.api.usersroles.v1.model.KeycloakRoleDTO keycloakRoleDTO);

    eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO toV0(RoleDTO roleDTO);

    List<KeycloakRoleDTO> toV0(List<eu.europa.ec.simpl.api.usersroles.v1.model.KeycloakRoleDTO> keycloakRoleDTO);
}
