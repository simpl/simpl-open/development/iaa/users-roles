package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import java.util.Collections;
import java.util.List;
import lombok.Generated;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper
@AnnotateWith(Generated.class)
public interface KeycloakMapper {

    @Mapping(source = "password", target = "credentials")
    @Mapping(target = "enabled", constant = "true")
    @Mapping(target = "emailVerified", constant = "true")
    UserRepresentation toRepresentation(KeycloakUserDTO userDTO);

    @Mapping(target = "roles", source = "roleList")
    KeycloakUserDTO toDto(UserRepresentation userRepresentation, List<String> roleList);

    @Mapping(target = "credentials", ignore = true)
    @Mapping(target = "username", ignore = true)
    void updateEntity(KeycloakUserDTO dto, @MappingTarget UserRepresentation entity);

    default List<CredentialRepresentation> passwordToCredentialRepresentation(String password) {
        var credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setTemporary(false);
        credentialRepresentation.setValue(password);
        return Collections.singletonList(credentialRepresentation);
    }

    @Mapping(target = "clientRole", constant = "true")
    RoleRepresentation toRepresentation(KeycloakRoleDTO roleDTO);

    KeycloakRoleDTO toDto(RoleRepresentation roleRepresentation);
}
