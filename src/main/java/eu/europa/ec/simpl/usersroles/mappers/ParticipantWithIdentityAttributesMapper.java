package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantWithIdentityAttributesDTO;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface ParticipantWithIdentityAttributesMapper {

    @Mapping(target = "participant", source = ".")
    @Mapping(target = "identityAttributes", source = "identityAttributes")
    @Mapping(target = "participant.id", source = "id")
    @Mapping(target = "participant.participantType", source = "participantType")
    @Mapping(target = "participant.organization", source = "organization")
    @Mapping(target = "participant.creationTimestamp", source = "creationTimestamp")
    @Mapping(target = "participant.updateTimestamp", source = "updateTimestamp")
    @Mapping(target = "participant.credentialId", source = "credentialId")
    @Mapping(target = "participant.expiryDate", source = "expiryDate")
    ParticipantWithIdentityAttributesDTO toDto(
            eu.europa.ec.simpl.api.authenticationprovider.v1.model.ParticipantWithIdentityAttributesDTO pingAgent);
}
