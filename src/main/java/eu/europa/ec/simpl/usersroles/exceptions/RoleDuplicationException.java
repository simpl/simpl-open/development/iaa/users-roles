package eu.europa.ec.simpl.usersroles.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class RoleDuplicationException extends StatusException {

    public RoleDuplicationException() {
        this(HttpStatus.CONFLICT, "Role already exists");
    }

    public RoleDuplicationException(HttpStatus status, String message) {
        super(status, message);
    }
}
