package eu.europa.ec.simpl.usersroles.repositories;

import eu.europa.ec.simpl.usersroles.entities.IdentityAttributeRoles;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IdentityAttributeRolesRepository
        extends JpaRepository<IdentityAttributeRoles, UUID>, JpaSpecificationExecutor<IdentityAttributeRoles> {

    @Modifying
    @Query(
            "update IdentityAttributeRoles iar set enabled = case when (iar.idaCode in :idaCodes) then true else false end")
    void updateAssignments(List<String> idaCodes);

    List<IdentityAttributeRoles> findByRoleNameAndEnabledTrue(String roleName);

    @Query(
            """
            select distinct iar.idaCode
            from IdentityAttributeRoles iar
            where iar.roleName in :roleNames and iar.enabled = true
            """)
    List<String> findDistinctEnabledIdaCodesByRoleNames(@Param("roleNames") List<String> roleNames);

    Optional<IdentityAttributeRoles> findByRoleNameAndIdaCode(String roleName, String idaCode);

    void deleteByRoleName(String roleName);

    long deleteByRoleNameAndIdaCode(String roleName, String idaCode);

    @Modifying
    @Query(
            """
            update IdentityAttributeRoles iar
            set iar.enabled = false
            where iar.roleName = :roleName
            """)
    void disableMappingByRoleName(String roleName);

    default boolean existsAtLeastOne() {
        return count() > 0;
    }
}
