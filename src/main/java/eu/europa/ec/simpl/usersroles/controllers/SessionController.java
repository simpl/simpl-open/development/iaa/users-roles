package eu.europa.ec.simpl.usersroles.controllers;

import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.SessionsApi;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.TierOneSessionDTO;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeMapper;
import eu.europa.ec.simpl.usersroles.mappers.TierOneSessionMapper;
import eu.europa.ec.simpl.usersroles.services.KeycloakUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("session")
public class SessionController {

    private final KeycloakUserService userService;
    private final SessionsApi sessionsApi;
    private final IdentityAttributeMapper identityAttributeMapper;
    private final TierOneSessionMapper tierOneSessionMapper;

    public SessionController(
            KeycloakUserService userService,
            SessionsApi sessionsApi,
            IdentityAttributeMapper identityAttributeMapper,
            TierOneSessionMapper tierOneSessionMapper) {
        this.userService = userService;
        this.sessionsApi = sessionsApi;
        this.identityAttributeMapper = identityAttributeMapper;
        this.tierOneSessionMapper = tierOneSessionMapper;
    }

    @Operation(
            summary = "Retrieve identity attributes of a participant",
            description = "Fetches the identity attributes associated with the specified participant ID",
            parameters = {
                @Parameter(
                        name = "credentialId",
                        description = "The Public Key Hash of the participant",
                        required = true,
                        schema = @Schema(type = "string"))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved identity attributes",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        array =
                                                @ArraySchema(
                                                        schema =
                                                                @Schema(implementation = IdentityAttributeDTO.class)))),
                @ApiResponse(responseCode = "404", description = "Ephemeral proof not found")
            })
    @GetMapping("{credentialId}")
    public List<IdentityAttributeDTO> getIdentityAttributesOfParticipant(@PathVariable String credentialId) {
        return identityAttributeMapper.toListDto(sessionsApi.getIdentityAttributesOfParticipant(credentialId));
    }

    @Operation(
            summary = "Validate Tier 1 session",
            description = "Validate the tier one session against the ephemeral proof stored in the agent",
            responses = {
                @ApiResponse(responseCode = "204", description = "Tier 1 session validated successfully"),
                @ApiResponse(responseCode = "422", description = "Invalid Tier 1 session")
            })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping("credential")
    public void validateTierOneSession(@RequestBody TierOneSessionDTO session) {
        sessionsApi.validateTierOneSession(tierOneSessionMapper.toAuthenticationProviderDTO(session));
    }

    @Operation(
            summary = "Current User Logout",
            description = "End the session for the current user",
            responses = {
                @ApiResponse(responseCode = "204", description = "Successfully deleted user session"),
                @ApiResponse(responseCode = "400", description = "Communication error with Keycloak admin"),
            })
    @DeleteMapping("current")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCurrentSession() {
        userService.logout();
    }
}
