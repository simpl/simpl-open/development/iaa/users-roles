package eu.europa.ec.simpl.usersroles.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "database-seeding")
public record DBSeedingProperties(RoleAttributesMapping roleAttributesMapping) {
    public record RoleAttributesMapping(boolean enabled, String filePath) {}
}
