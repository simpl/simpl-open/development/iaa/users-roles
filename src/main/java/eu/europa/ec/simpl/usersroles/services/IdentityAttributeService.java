package eu.europa.ec.simpl.usersroles.services;

import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.usersroles.filters.IdentityAttributeWithOwnershipFilter;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IdentityAttributeService {
    Page<IdentityAttributeWithOwnershipDTO> search(IdentityAttributeWithOwnershipFilter request, Pageable pageable);

    @Transactional
    void overwriteIdentityAttributes(List<IdentityAttributeWithOwnershipDTO> identityAttributes);

    @Transactional
    void updateAssignedIdentityAttributes(List<IdentityAttributeDTO> idaFromEphemeralProof);

    Set<IdentityAttributeDTO> getIdentityAttributesWithCodeIn(List<String> identityAttributesCode);
}
