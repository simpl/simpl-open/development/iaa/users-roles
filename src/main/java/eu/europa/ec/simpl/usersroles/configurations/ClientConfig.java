package eu.europa.ec.simpl.usersroles.configurations;

import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.AgentsApi;
import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.CertificateSignRequestsApi;
import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.CredentialsApi;
import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.IdentityAttributesApi;
import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.MtlsApi;
import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.SessionsApi;
import eu.europa.ec.simpl.client.core.AuthenticationProviderConfig;
import eu.europa.ec.simpl.client.okhttp.DaggerOkHttpSimplClientFactory;
import eu.europa.ec.simpl.client.okhttp.OkHttpSimplClient;
import eu.europa.ec.simpl.common.argumentresolvers.PageableArgumentResolver;
import eu.europa.ec.simpl.common.argumentresolvers.QueryParamsArgumentResolver;
import eu.europa.ec.simpl.common.exchanges.authenticationprovider.KeyPairExchange;
import eu.europa.ec.simpl.common.exchanges.mtls.AuthorityExchange;
import eu.europa.ec.simpl.common.interceptors.TierOneTokenPropagatorInterceptor;
import eu.europa.ec.simpl.common.messageconverters.StreamingResponseBodyMessageConverter;
import eu.europa.ec.simpl.common.utils.OkHttpClientHttpRequestFactory;
import java.net.URI;
import java.util.function.Supplier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.JdkClientHttpRequestFactory;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Configuration
public class ClientConfig {

    private static final String AUTHENTICATION_PROVIDER_URL_VERSION = "/v1";

    private final MicroserviceProperties properties;

    public ClientConfig(MicroserviceProperties properties) {
        this.properties = properties;
    }

    @Bean
    public CertificateSignRequestsApi certificateSignRequestsApi(RestClient.Builder restClientBuilder) {
        return buildExchange(
                new JdkClientHttpRequestFactory(),
                properties.authenticationProvider().url().resolve(AUTHENTICATION_PROVIDER_URL_VERSION),
                restClientBuilder.messageConverters(
                        converters -> converters.add(new StreamingResponseBodyMessageConverter())),
                CertificateSignRequestsApi.class);
    }

    @Bean
    public MtlsApi mtlsApi(RestClient.Builder restClientBuilder) {
        return buildExchange(
                new JdkClientHttpRequestFactory(),
                properties.authenticationProvider().url().resolve(AUTHENTICATION_PROVIDER_URL_VERSION),
                restClientBuilder,
                MtlsApi.class);
    }

    @Bean
    public AgentsApi agentsApi(RestClient.Builder restClientBuilder) {
        return buildExchange(
                new JdkClientHttpRequestFactory(),
                properties.authenticationProvider().url().resolve(AUTHENTICATION_PROVIDER_URL_VERSION),
                restClientBuilder,
                AgentsApi.class);
    }

    @Bean
    public SessionsApi sessionsApi(RestClient.Builder restClientBuilder) {
        return buildExchange(
                new JdkClientHttpRequestFactory(),
                properties.authenticationProvider().url().resolve(AUTHENTICATION_PROVIDER_URL_VERSION),
                restClientBuilder,
                SessionsApi.class);
    }

    @Bean
    public CredentialsApi credentialsApi(RestClient.Builder restClientBuilder) {
        return buildExchange(
                new JdkClientHttpRequestFactory(),
                properties.authenticationProvider().url().resolve(AUTHENTICATION_PROVIDER_URL_VERSION),
                restClientBuilder.messageConverters(
                        converters -> converters.add(new StreamingResponseBodyMessageConverter())),
                CredentialsApi.class);
    }

    @Bean
    public IdentityAttributesApi identityAttributesApi(RestClient.Builder restClientBuilder) {
        return buildExchange(
                new JdkClientHttpRequestFactory(),
                properties.authenticationProvider().url().resolve(AUTHENTICATION_PROVIDER_URL_VERSION),
                restClientBuilder,
                IdentityAttributesApi.class);
    }

    @Bean
    public KeyPairExchange keyPairExchange(RestClient.Builder restClientBuilder) {
        return buildExchange(
                new JdkClientHttpRequestFactory(),
                properties.authenticationProvider().url().resolve(AUTHENTICATION_PROVIDER_URL_VERSION),
                restClientBuilder,
                KeyPairExchange.class);
    }

    @Bean
    public OkHttpSimplClient okHttpSimplClient() {
        return DaggerOkHttpSimplClientFactory.create().get();
    }

    @Bean
    public Supplier<AuthorityExchange> authorityExchange(
            OkHttpSimplClient okHttpSimplClient,
            TierOneTokenPropagatorInterceptor tierOneTokenPropagatorInterceptor,
            MtlsClientProperties mtlsClientProperties,
            RestClient.Builder restClientBuilder) {
        return () -> buildAuthorityExchange(
                okHttpSimplClient, tierOneTokenPropagatorInterceptor, mtlsClientProperties, restClientBuilder);
    }

    private AuthorityExchange buildAuthorityExchange(
            OkHttpSimplClient okHttpSimplClient,
            TierOneTokenPropagatorInterceptor tierOneTokenPropagatorInterceptor,
            MtlsClientProperties mtlsClientProperties,
            RestClient.Builder restClientBuilder) {
        var okHttpClientBuilder = okHttpSimplClient
                .builder()
                .setAuthorizationHeaderSupplier(tierOneTokenPropagatorInterceptor)
                .setAuthenticationProviderConfigSupplier(() -> new AuthenticationProviderConfig(
                        properties.authenticationProvider().url()))
                .build();
        return buildExchange(
                new OkHttpClientHttpRequestFactory(okHttpClientBuilder.build()),
                mtlsClientProperties.authority().url(),
                restClientBuilder,
                AuthorityExchange.class);
    }

    private static <E> E buildExchange(
            ClientHttpRequestFactory requestFactory,
            URI baseurl,
            RestClient.Builder restClientBuilder,
            Class<E> clazz) {
        var restClient = restClientBuilder
                .baseUrl(baseurl)
                .requestFactory(requestFactory)
                .build();
        var adapter = RestClientAdapter.create(restClient);
        var factory = HttpServiceProxyFactory.builderFor(adapter)
                .customArgumentResolver(new PageableArgumentResolver())
                .customArgumentResolver(new QueryParamsArgumentResolver())
                .build();
        return factory.createClient(clazz);
    }
}
