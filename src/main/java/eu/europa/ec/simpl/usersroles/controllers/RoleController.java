package eu.europa.ec.simpl.usersroles.controllers;

import eu.europa.ec.simpl.common.exchanges.usersroles.RoleExchange;
import eu.europa.ec.simpl.common.filters.RoleFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import eu.europa.ec.simpl.usersroles.services.RoleService;
import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class RoleController implements RoleExchange {
    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public RoleDTO findById(@PathVariable("id") UUID roleId) {
        log.info("Received GET request for role with id: {}", roleId);
        return roleService.findById(roleId);
    }

    @Override
    public RoleDTO create(@RequestBody @Valid KeycloakRoleDTO roleDTO) {
        log.info("Received POST request for role: {}", roleDTO);
        return roleService.create(roleDTO);
    }

    @Override
    public RoleDTO update(@RequestBody @Valid RoleDTO roleDTO) {
        log.info("Received PUT request for role: {}", roleDTO);
        return roleService.update(roleDTO);
    }

    @Override
    public void replaceIdentityAttributes(
            @PathVariable("id") UUID roleId, @RequestBody List<String> identityAttributesCodes) {
        roleService.replaceIdentityAttributes(roleId, identityAttributesCodes);
    }

    @Override
    public void deleteAttributeFromRole(@RequestParam UUID roleId, @RequestParam String attributeCode) {
        log.info("Received DELETE request for attribute: {} from role: {} ", attributeCode, roleId);
        roleService.removeAttributeForRole(attributeCode, roleId);
    }

    @Override
    public void delete(@PathVariable("roleId") UUID roleId) {
        log.info("Received DELETE request for role with id: {}", roleId);
        roleService.delete(roleId);
    }

    @Override
    public PageResponse<RoleDTO> search(
            @ParameterObject RoleFilter filter, @PageableDefault(sort = "id") @ParameterObject Pageable pageable) {
        log.info("Received GET /search request for roles");
        return new PageResponse<>(roleService.search(filter, pageable));
    }

    @Override
    public void duplicateIdentityAttributeToAnOtherRole(
            @PathVariable("id") UUID sourceRoleId, @RequestBody String destinationRoleId) {
        roleService.duplicateIdentityAttributeToAnOtherRole(sourceRoleId, UUID.fromString(destinationRoleId));
    }

    @Override
    public List<String> getIdentityAttributesFromRoleList(@RequestBody List<String> roleNameList) {
        return roleService.findIdentityAttributesForUser(roleNameList);
    }

    @Override
    public void importRoles(@Valid @RequestBody List<KeycloakRoleDTO> roles) {
        log.info("Received POST request for importing [{}] roles", roles.size());
        roleService.importRoles(roles);
    }
}
