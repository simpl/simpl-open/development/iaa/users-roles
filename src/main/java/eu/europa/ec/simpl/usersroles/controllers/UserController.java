package eu.europa.ec.simpl.usersroles.controllers;

import eu.europa.ec.simpl.common.constants.Roles;
import eu.europa.ec.simpl.common.exchanges.usersroles.UserExchange;
import eu.europa.ec.simpl.common.filters.KeycloakUserFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.usersroles.exceptions.EmptyRolesException;
import eu.europa.ec.simpl.usersroles.services.KeycloakUserService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class UserController implements UserExchange {

    private final KeycloakUserService userService;

    public UserController(KeycloakUserService userService) {
        this.userService = userService;
    }

    @Override
    public String createUser(KeycloakUserDTO userDTO) {
        // internal request in the onboarding phase of a participant
        log.info("Received internal POST request for user with email [{}]", userDTO);
        if (userDTO.getRoles().isEmpty()) {
            userDTO.getRoles().add(Roles.APPLICANT);
        }
        return userService.createUser(userDTO);
    }

    @Override
    public String createUserAsT1(@RequestBody @Valid KeycloakUserDTO userDTO) {
        log.info("Received POST request for user with email [{}]", userDTO);
        if (userDTO.getRoles().isEmpty()) {
            throw new EmptyRolesException(HttpStatus.BAD_REQUEST, "Roles cannot be empty");
        }
        return userService.createUser(userDTO);
    }

    @Override
    public List<KeycloakRoleDTO> getRoles(@PathVariable String uuid) {
        log.info("Received GET request for roles of user with uuid [{}]", uuid);
        return userService.getUserRoles(uuid);
    }

    @Override
    public KeycloakUserDTO getUser(@RequestParam @Valid @Email String email) {
        log.info("Received GET request for user with userId [{}]", email);
        return userService.getUserByEmail(email);
    }

    @Override
    public KeycloakUserDTO getUserByUuid(@PathVariable String uuid) {
        log.info("Received GET request for user with uuid [{}]", uuid);
        return userService.getUserByUuid(uuid);
    }

    @Override
    public void updateUser(@PathVariable String uuid, @RequestBody @Valid KeycloakUserDTO userDTO) {
        log.info("Received PUT request for user with uuid [{}]", uuid);
        userService.updateUser(uuid, userDTO);
    }

    @Override
    public void updateUserRoles(@PathVariable String uuid, @RequestBody List<String> userRoles) {
        log.info("Received PUT request for user-roles of user with uuid [{}]", uuid);
        userService.updateUserRoles(uuid, userRoles);
    }

    @Override
    public void deleteUser(@PathVariable String uuid) {
        log.info("Received DELETE request for user with uuid [{}]", uuid);
        userService.deleteUser(uuid);
    }

    @Override
    public List<KeycloakUserDTO> search(@ParameterObject KeycloakUserFilter filter) {
        return userService.search(filter);
    }

    @Override
    public void importUsers(@Valid @RequestBody List<KeycloakUserDTO> users) {
        log.info("Received POST request for importing [{}] users and relative roles", users.size());
        userService.importUsers(users);
    }
}
