package eu.europa.ec.simpl.usersroles;

import eu.europa.ec.simpl.common.messaging.EnableMessageConsumer;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantWithIdentityAttributesDTO;
import org.springdoc.core.utils.SpringDocUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@ConfigurationPropertiesScan
@SpringBootApplication
@EnableMessageConsumer
public class UsersRolesApplication {
    public static void main(String[] args) {
        SpringDocUtils.getConfig().addParentType(ParticipantWithIdentityAttributesDTO.class.getSimpleName());
        SpringApplication.run(UsersRolesApplication.class, args);
    }
}
