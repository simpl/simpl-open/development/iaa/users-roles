package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.api.authenticationprovider.v1.model.SearchIdentityAttributesWithOwnershipFilterParameterDTO;
import eu.europa.ec.simpl.usersroles.filters.IdentityAttributeWithOwnershipFilter;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface SearchIdentityAttributesWithOwnershipFilterParameterMapper {

    SearchIdentityAttributesWithOwnershipFilterParameterDTO toDTO(IdentityAttributeWithOwnershipFilter filter);
}
