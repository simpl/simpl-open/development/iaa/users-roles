package eu.europa.ec.simpl.usersroles.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class KeycloakRoleException extends StatusException {

    public KeycloakRoleException(String e) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }
}
