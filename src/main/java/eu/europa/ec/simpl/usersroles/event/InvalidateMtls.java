package eu.europa.ec.simpl.usersroles.event;

import eu.europa.ec.simpl.usersroles.entities.Credential;
import jakarta.annotation.Nullable;
import lombok.Value;

@Value
public class InvalidateMtls {
    @Nullable Credential newCredential;
}
