package eu.europa.ec.simpl.usersroles.entities;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Table;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "identity_attribute")
@Accessors(chain = true)
@Getter
@Setter
@ToString
public class IdentityAttributeWithOwnership {

    @Id
    private UUID id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "assignable_to_roles")
    private boolean assignableToRoles;

    @Column(name = "assigned_to_participant")
    private boolean assignedToParticipant;

    @Column(name = "enabled")
    private boolean enabled;

    @ElementCollection
    @CollectionTable(
            name = "identity_attribute_participant_type",
            joinColumns = @JoinColumn(name = "identity_attribute_id"))
    @Column(name = "participant_type")
    @ToString.Exclude
    private Set<String> participantTypes = new HashSet<>();

    @CreationTimestamp
    @Column(name = "creation_timestamp")
    private Instant creationTimestamp;

    @UpdateTimestamp
    @Column(name = "update_timestamp")
    private Instant updateTimestamp;
}
