package eu.europa.ec.simpl.usersroles.services.impl;

import eu.europa.ec.simpl.common.exchanges.authenticationprovider.KeyPairExchange;
import eu.europa.ec.simpl.common.utils.CredentialUtil;
import eu.europa.ec.simpl.usersroles.services.KeyPairService;
import java.security.PrivateKey;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

@Service
public class KeyPairServiceImpl implements KeyPairService {

    private final KeyPairExchange exchange;

    public KeyPairServiceImpl(KeyPairExchange exchange) {
        this.exchange = exchange;
    }

    @Override
    @SneakyThrows
    public PrivateKey getPrivateKey() {
        var privateKey = exchange.getInstalledKeyPair().getPrivateKey();
        return CredentialUtil.loadPrivateKey(privateKey, "EC");
    }
}
