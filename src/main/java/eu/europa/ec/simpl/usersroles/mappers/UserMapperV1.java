package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.api.usersroles.v1.model.KeycloakRoleDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.KeycloakUserDTO;
import eu.europa.ec.simpl.common.filters.KeycloakUserFilter;
import java.util.List;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface UserMapperV1 {
    List<KeycloakRoleDTO> toV1(List<eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO> roles);

    KeycloakUserDTO toV1(eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO user);

    KeycloakUserFilter toKeycloakUserFilter(
            String username, String firstName, String lastName, String email, Integer first, Integer max);

    List<KeycloakUserDTO> toListKeycloakUserDTOV1(
            List<eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO> search);

    eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO toV0(KeycloakUserDTO keycloakUserDTO);

    List<eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO> toV0(List<KeycloakUserDTO> keycloakUserDTO);
}
