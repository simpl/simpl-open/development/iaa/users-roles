package eu.europa.ec.simpl.usersroles.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@Table(name = "identity_attribute_roles")
@Accessors(chain = true)
@Getter
@Setter
@ToString
public class IdentityAttributeRoles {

    @Id
    @UUIDv7Generator
    private UUID id;

    @Column(name = "ida_code")
    private String idaCode;

    @Column(name = "role_name")
    private String roleName;

    @Column(name = "enabled")
    private Boolean enabled;
}
