package eu.europa.ec.simpl.usersroles.controllers;

import eu.europa.ec.simpl.api.usersroles.v1.exchanges.AgentsApi;
import eu.europa.ec.simpl.api.usersroles.v1.model.EchoDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.IdentityAttributeDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.ParticipantWithIdentityAttributesDTO;
import eu.europa.ec.simpl.usersroles.mappers.AgentMapperV1;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
public class AgentControllerV1 implements AgentsApi {

    private final AgentController controller;
    private final AgentMapperV1 mapper;

    public AgentControllerV1(AgentController controller, AgentMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public EchoDTO echo() {
        return mapper.toV1(controller.echo());
    }

    @Override
    public List<IdentityAttributeWithOwnershipDTO> getIdentityAttributesWithOwnership() {
        return mapper.toListIdentityAttributeWithOwnershipDTOV1(controller.getIdentityAttributesWithOwnership());
    }

    @Override
    public List<IdentityAttributeDTO> getParticipantIdentityAttributes(String certificateId, String credentialId) {
        return mapper.toListIdentityAttributeDTOV1(controller.getParticipantIdentityAttributes(credentialId));
    }

    @Override
    public ParticipantWithIdentityAttributesDTO pingAgent(String fqdn) {
        return mapper.toV1(controller.ping(fqdn));
    }
}
