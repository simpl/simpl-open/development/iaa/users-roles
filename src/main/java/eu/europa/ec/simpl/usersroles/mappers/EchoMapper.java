package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.common.model.dto.usersroles.EchoDTO;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface EchoMapper {

    @Mapping(target = "participant.participant", source = "creationTimestamp")
    @Mapping(target = "participant.participant.credentialId", source = "credentialId")
    @Mapping(target = "participant.participant.expiryDate", source = "expiryDate")
    @Mapping(target = "participant.identityAttributes", source = "identityAttributes")
    @Mapping(target = "participant.participant.updateTimestamp", source = "updateTimestamp")
    @Mapping(target = "participant.participant.id", source = "id")
    @Mapping(target = "participant.participant.participantType", source = "participantType")
    @Mapping(target = "participant.participant.organization", source = "organization")
    EchoDTO toDto(eu.europa.ec.simpl.api.authenticationprovider.v1.model.EchoDTO echo);
}
