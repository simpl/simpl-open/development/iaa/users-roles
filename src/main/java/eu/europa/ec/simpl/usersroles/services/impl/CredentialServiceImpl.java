package eu.europa.ec.simpl.usersroles.services.impl;

import eu.europa.ec.simpl.usersroles.entities.Credential;
import eu.europa.ec.simpl.usersroles.exceptions.CredentialsNotFoundException;
import eu.europa.ec.simpl.usersroles.repositories.CredentialRepository;
import eu.europa.ec.simpl.usersroles.services.CredentialService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class CredentialServiceImpl implements CredentialService {

    private final CredentialRepository credentialRepository;

    public CredentialServiceImpl(CredentialRepository credentialRepository) {
        this.credentialRepository = credentialRepository;
    }

    @Override
    public byte[] getCredential() {
        log.info("CredentialService.getCredential() start");
        return credentialRepository.findAll().stream()
                .findAny()
                .map(Credential::getContent)
                .orElseThrow(CredentialsNotFoundException::new);
    }
}
