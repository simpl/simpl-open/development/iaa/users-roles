package eu.europa.ec.simpl.usersroles.configurations;

import java.net.URI;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "client")
public record MtlsClientProperties(AuthorityProperties authority) {

    public record AuthorityProperties(URI url) {}
}
