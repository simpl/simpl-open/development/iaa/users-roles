package eu.europa.ec.simpl.usersroles.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.UUID;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class IdentityAttributeAlreadyAssignedToRoleException extends StatusException {

    private final UUID roleId;
    private final UUID identityAttributeId;

    public IdentityAttributeAlreadyAssignedToRoleException(UUID roleId, UUID identityAttributeId, String message) {
        super(HttpStatus.CONFLICT, message);
        this.roleId = roleId;
        this.identityAttributeId = identityAttributeId;
    }
}
