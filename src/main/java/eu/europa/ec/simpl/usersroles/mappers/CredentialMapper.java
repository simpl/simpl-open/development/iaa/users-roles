package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.common.model.dto.usersroles.CredentialDTO;
import eu.europa.ec.simpl.usersroles.entities.Credential;
import java.io.IOException;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.web.multipart.MultipartFile;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface CredentialMapper {

    @Mapping(target = "content", source = "file")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "participantId", ignore = true)
    Credential toEntity(MultipartFile file);

    @BeanMapping(ignoreUnmappedSourceProperties = {"credentialId", "participantId"})
    CredentialDTO toCredentialDTO(eu.europa.ec.simpl.api.authenticationprovider.v1.model.CredentialDTO credentialDTO);

    default byte[] getBytes(MultipartFile file) throws IOException {
        return file.getBytes();
    }
}
