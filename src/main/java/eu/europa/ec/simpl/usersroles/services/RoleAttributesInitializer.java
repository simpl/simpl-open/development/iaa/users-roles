package eu.europa.ec.simpl.usersroles.services;

public interface RoleAttributesInitializer {
    void init();
}
