package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.usersroles.entities.IdentityAttributeRoles;
import java.util.List;
import java.util.UUID;
import lombok.Generated;
import org.keycloak.representations.idm.RoleRepresentation;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
@AnnotateWith(Generated.class)
public interface RoleMapper {

    @Mapping(source = "representation.name", target = "name")
    @Mapping(source = "representation.description", target = "description")
    @Mapping(source = "representation", target = "id")
    @Mapping(source = "idas", target = "assignedIdentityAttributes")
    RoleDTO toDto(RoleRepresentation representation, List<IdentityAttributeRoles> idas);

    default List<String> roleIdaToRoleDTOIda(List<IdentityAttributeRoles> rolesIda) {
        if (rolesIda != null) {
            return rolesIda.stream().map(IdentityAttributeRoles::getIdaCode).toList();
        } else {
            return null;
        }
    }

    default UUID getUuid(RoleRepresentation representation) {
        return UUID.fromString(representation.getId());
    }
}
