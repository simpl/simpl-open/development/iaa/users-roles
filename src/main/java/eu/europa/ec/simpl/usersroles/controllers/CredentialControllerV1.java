package eu.europa.ec.simpl.usersroles.controllers;

import eu.europa.ec.simpl.api.usersroles.v1.exchanges.CredentialsApi;
import eu.europa.ec.simpl.api.usersroles.v1.model.CredentialDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.ParticipantDTO;
import eu.europa.ec.simpl.usersroles.mappers.CredentialMapperV1;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@RestController
@RequestMapping("v1")
public class CredentialControllerV1 implements CredentialsApi {

    private final CredentialController controller;
    private final CredentialMapperV1 mapper;

    public CredentialControllerV1(CredentialController controller, CredentialMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public void delete() {
        controller.delete();
    }

    @Override
    public StreamingResponseBody downloadInstalledCredentials() {
        return controller.downloadInstalledCredentials();
    }

    @Override
    public ParticipantDTO getCredentialId() {
        return mapper.toV1(controller.getCredentialId());
    }

    @Override
    public ParticipantDTO getMyParticipantId() {
        return mapper.toV1(controller.getMyParticipantId());
    }

    @Override
    public CredentialDTO getPublicKey() {
        return mapper.toV1(controller.getPublicKey());
    }

    @Override
    public Boolean hasCredential() {
        return controller.hasCredential();
    }

    @Override
    public Integer uploadCredential(MultipartFile file) {
        return ((Number) controller.uploadCredential(file)).intValue();
    }
}
