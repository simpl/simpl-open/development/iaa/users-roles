package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.api.usersroles.v1.model.CredentialDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.ParticipantDTO;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface CredentialMapperV1 {
    ParticipantDTO toV1(eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantDTO credentialId);

    CredentialDTO toV1(eu.europa.ec.simpl.common.model.dto.usersroles.CredentialDTO publicKey);
}
