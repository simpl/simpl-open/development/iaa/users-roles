package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import java.util.List;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface IdentityAttributeMapper {

    IdentityAttributeDTO toDto(
            eu.europa.ec.simpl.api.authenticationprovider.v1.model.IdentityAttributeDTO participantIdentityAttributes);

    default List<IdentityAttributeDTO> toListDto(
            List<eu.europa.ec.simpl.api.authenticationprovider.v1.model.IdentityAttributeDTO>
                    participantIdentityAttributes) {
        if (participantIdentityAttributes != null) {
            return participantIdentityAttributes.stream().map(this::toDto).toList();
        } else {
            return null;
        }
    }
}
