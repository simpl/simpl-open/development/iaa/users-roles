package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantDTO;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface ParticipantMapper {

    ParticipantDTO toParticipantDTO(
            eu.europa.ec.simpl.api.authenticationprovider.v1.model.ParticipantDTO myParticipantId);
}
