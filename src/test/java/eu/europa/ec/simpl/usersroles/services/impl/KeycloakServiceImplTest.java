package eu.europa.ec.simpl.usersroles.services.impl;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.mockito.BDDMockito.*;

import eu.europa.ec.simpl.usersroles.configurations.keycloak.KeycloakProperties;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.ClientRepresentation;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class KeycloakServiceImplTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    KeycloakProperties properties;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    Keycloak keycloak;

    @InjectMocks
    KeycloakServiceImpl keycloakService;

    @Test
    void getAppRealm() {
        // Given
        given(properties.app().realm()).willReturn("realm");
        // When
        keycloakService.getAppRealm();
        // Then
        then(keycloak).should().realm("realm");
    }

    @Test
    void getAppUsers() {
        // Given
        given(properties.app().realm()).willReturn("realm");
        // When
        keycloakService.getAppUsers();
        // Then
        then(keycloak.realm("realm")).should().users();
    }

    @Test
    void getClientRoleScope() {
        var userId = UUID.randomUUID();
        var clientRepresentation = a(ClientRepresentation.class);

        // Given
        given(properties.app().realm()).willReturn("realm");
        given(properties.app().clientId()).willReturn("frontend-cli");
        given(keycloak.realm("realm").clients().findByClientId("frontend-cli"))
                .willReturn(List.of(clientRepresentation));
        // When
        keycloakService.getClientRoleScope(userId.toString());
        // Then
        then(keycloak.realm("realm").users().get(userId.toString()).roles())
                .should()
                .clientLevel(clientRepresentation.getId());
    }

    @Test
    void getClientRoles() {
        var clientRepresentation = a(ClientRepresentation.class);

        // Given
        given(properties.app().realm()).willReturn("realm");
        given(properties.app().clientId()).willReturn("frontend-cli");
        given(keycloak.realm("realm").clients().findByClientId("frontend-cli"))
                .willReturn(List.of(clientRepresentation));
        // When
        keycloakService.getClientRoles();
        // Then
        then(keycloak.realm("realm").clients()).should().get(clientRepresentation.getId());
    }

    @Test
    void getClientUUID() {
        var clientRepresentation = a(ClientRepresentation.class);
        // Given
        given(properties.app().realm()).willReturn("realm");
        given(properties.app().clientId()).willReturn("frontend-cli");
        given(keycloak.realm("realm").clients().findByClientId("frontend-cli"))
                .willReturn(List.of(clientRepresentation));
        // When
        keycloakService.getClientUUID();
        // Then
        then(keycloak.realm("realm").clients()).should().findByClientId("frontend-cli");
    }
}
