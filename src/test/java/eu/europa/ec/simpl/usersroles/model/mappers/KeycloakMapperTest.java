package eu.europa.ec.simpl.usersroles.model.mappers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.usersroles.mappers.KeycloakMapper;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.keycloak.representations.idm.UserRepresentation;
import org.mapstruct.factory.Mappers;

class KeycloakMapperTest {
    private KeycloakMapper mapper;

    @BeforeEach
    void setup() {
        mapper = Mappers.getMapper(KeycloakMapper.class);
    }

    @Test
    void updateEntity_successMapper() {
        var dto = a(KeycloakUserDTO.class);
        var entity = new UserRepresentation();
        entity.setCredentials(new ArrayList<>());
        assertDoesNotThrow(() -> mapper.updateEntity(dto, entity));
    }

    @Test
    void updateEntity_nullValues() {
        var dto = a(KeycloakUserDTO.class);
        var entity = new UserRepresentation();
        assertDoesNotThrow(() -> mapper.updateEntity(dto, entity));
        assertDoesNotThrow(() -> mapper.updateEntity(null, entity));
    }
}
