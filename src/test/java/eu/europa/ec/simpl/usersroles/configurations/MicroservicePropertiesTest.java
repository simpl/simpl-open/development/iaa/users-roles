package eu.europa.ec.simpl.usersroles.configurations;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties(MicroserviceProperties.class)
@TestPropertySource(
        properties = {
            "microservice.authentication-provider.url=http://localhost:8081",
        })
class MicroservicePropertiesTest {

    @Autowired
    MicroserviceProperties properties;

    @Test
    void validateMicroserviceProperties() {
        Assertions.assertThat(properties).hasNoNullFieldsOrProperties();
    }
}
