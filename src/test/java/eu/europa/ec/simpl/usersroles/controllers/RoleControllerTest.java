package eu.europa.ec.simpl.usersroles.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.filters.RoleFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.usersroles.exceptions.RoleNotFoundException;
import eu.europa.ec.simpl.usersroles.services.RoleService;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;

@ExtendWith(MockitoExtension.class)
class RoleControllerTest {

    @Mock
    private RoleService roleService;

    @InjectMocks
    private RoleController roleController;

    @Autowired
    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(roleController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    void testFindRoleById() throws Exception {
        var roleId = UUID.randomUUID();
        var expectedRole = a(RoleDTO.class);

        when(roleService.findById(roleId)).thenReturn(expectedRole);

        mvc.perform(get("/role/{roleName}", roleId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expectedRole.getId().toString()))
                .andExpect(jsonPath("$.name").value(expectedRole.getName()))
                .andExpect(jsonPath("$.description").value(expectedRole.getDescription()));

        verify(roleService, times(1)).findById(roleId);
    }

    @Test
    void testCreateRole() throws Exception {
        var input = a(KeycloakRoleDTO.class);
        var expectedRole = a(RoleDTO.class);
        given(roleService.create(any())).willReturn(expectedRole);

        mvc.perform(post("/role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(input)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(expectedRole.getId().toString()))
                .andExpect(jsonPath("$.name").value(expectedRole.getName()))
                .andExpect(jsonPath("$.description").value(expectedRole.getDescription()));

        then(roleService).should(times(1)).create(any(KeycloakRoleDTO.class));
    }

    @Test
    void testUpdateRole() throws Exception {
        var input = a(RoleDTO.class);
        var expectedRole = a(RoleDTO.class);
        given(roleService.update(any())).willReturn(expectedRole);

        mvc.perform(put("/role")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(input)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expectedRole.getId().toString()))
                .andExpect(jsonPath("$.name").value(expectedRole.getName()))
                .andExpect(jsonPath("$.description").value(expectedRole.getDescription()));

        then(roleService).should(times(1)).update(input);
    }

    @Test
    void testDeleteRole() throws Exception {
        var roleId = UUID.randomUUID();
        mvc.perform(delete("/role/{id}", roleId)).andExpect(status().isNoContent());
        then(roleService).should(times(1)).delete(roleId);
    }

    @Test
    void assignIdentityAttributes_usingValidUser_success() throws Exception {
        var roleId = UUID.randomUUID();
        var iaCodes = List.of("ia1", "ia2");

        mvc.perform(request(HttpMethod.PUT, "/role/{roleId}/identity-attributes", roleId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(iaCodes)))
                .andExpect(
                        result -> assertThat(result.getResponse().getStatus()).isEqualTo(200));

        verify(roleService).replaceIdentityAttributes(eq(roleId), argThat(iaCodes::containsAll));
    }

    @Test
    void assignIdentityAttributes_RoleDoesNotExist_404Response() throws Exception {
        var roleId = UUID.randomUUID();
        var iaCodes = List.of("ia1", "ia2");

        doThrow(new RoleNotFoundException("unit test mock")).when(roleService).replaceIdentityAttributes(any(), any());

        mvc.perform(request(HttpMethod.PUT, "/role/{roleId}/identity-attributes", roleId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(iaCodes)))
                .andExpect(
                        result -> assertThat(result.getResponse().getStatus()).isEqualTo(404));
    }

    @Test
    void deleteAttributeFromRole() throws Exception {
        var roleId = UUID.randomUUID();
        var attributeCode = "AttributeCode";
        var params = new LinkedMultiValueMap<String, String>();
        params.add("roleId", roleId.toString());
        params.add("attributeCode", attributeCode);

        mvc.perform(delete("/role/delete-attribute").queryParams(params)).andExpect(status().isNoContent());

        then(roleService).should(times(1)).removeAttributeForRole(attributeCode, roleId);
    }

    @Test
    void searchRoles() throws Exception {
        var expectedObj = a(RoleDTO.class).setName("role-1");
        var expectedResult = new PageImpl<>(List.of(expectedObj));
        given(roleService.search(any(RoleFilter.class), any(Pageable.class))).willReturn(expectedResult);

        mvc.perform(get("/role/search").param("name", "role-1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].name").value("role-1"));

        verify(roleService, times(1)).search(any(RoleFilter.class), any(Pageable.class));
    }

    @Test
    void duplicateIdentityAttributeToAnOtherRole_NoThrowException_successResponse() throws Exception {
        var sourceRoleId = UUID.randomUUID();
        var destinationRoleId = UUID.randomUUID();

        doNothing().when(roleService).duplicateIdentityAttributeToAnOtherRole(sourceRoleId, destinationRoleId);

        mvc.perform(request(HttpMethod.POST, "/role/{roleId}/duplicate-identity-attribute", sourceRoleId)
                        .accept(MediaType.TEXT_PLAIN)
                        .contentType(MediaType.TEXT_PLAIN)
                        .content(destinationRoleId.toString()))
                .andExpect(
                        result -> assertThat(result.getResponse().getStatus()).isEqualTo(200));
    }

    @Test
    void duplicateIdentityAttributeToAnOtherRole_ThrowUserNotFoundException_statusCode404() throws Exception {
        var sourceRoleId = UUID.randomUUID();
        var destinationRoleId = UUID.randomUUID();

        doThrow(new RoleNotFoundException("unit tests error"))
                .when(roleService)
                .duplicateIdentityAttributeToAnOtherRole(any(), any());

        mvc.perform(request(HttpMethod.POST, "/role/{roleId}/duplicate-identity-attribute", sourceRoleId)
                        .contentType(MediaType.TEXT_PLAIN)
                        .content(destinationRoleId.toString()))
                .andExpect(
                        result -> assertThat(result.getResponse().getStatus()).isEqualTo(404));
    }
}
