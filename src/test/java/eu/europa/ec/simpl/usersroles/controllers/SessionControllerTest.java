package eu.europa.ec.simpl.usersroles.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.mockito.Mockito.verify;

import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.SessionsApi;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeMapperImpl;
import eu.europa.ec.simpl.usersroles.mappers.TierOneSessionMapperImpl;
import eu.europa.ec.simpl.usersroles.services.KeycloakUserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({
    SessionController.class,
    IdentityAttributeMapperImpl.class,
    TierOneSessionMapperImpl.class,
})
class SessionControllerTest {

    @MockitoBean(answers = Answers.RETURNS_DEEP_STUBS)
    KeycloakUserService userService;

    @MockitoBean(answers = Answers.RETURNS_DEEP_STUBS)
    SessionsApi sessionsApi;

    @Autowired
    SessionController sessionController;

    @Test
    void getIdentityAttributesOfParticipant() {
        var credentialId = a(String.class);
        sessionController.getIdentityAttributesOfParticipant(credentialId);
        verify(sessionsApi).getIdentityAttributesOfParticipant(credentialId);
    }
}
