package eu.europa.ec.simpl.usersroles.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantWithIdentityAttributesDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.EchoDTO;
import eu.europa.ec.simpl.usersroles.mappers.AgentMapperV1Impl;
import eu.europa.ec.simpl.usersroles.utils.DtoUtils;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({
    AgentControllerV1.class,
    AgentMapperV1Impl.class,
})
public class AgentControllerV1Test {

    @MockitoBean
    private AgentController controller;

    @Autowired
    private AgentControllerV1 controllerV1;

    @Test
    void echoTest() {
        var resultC0 = a(EchoDTO.class);
        given(controller.echo()).willReturn(resultC0);
        var result = controllerV1.echo();
        assertThat(DtoUtils.jsonCompare(result, resultC0)).isTrue();
    }

    @Test
    void getIdentityAttributesWithOwnershipTest() {
        var resultC0 = List.of(a(IdentityAttributeWithOwnershipDTO.class));
        given(controller.getIdentityAttributesWithOwnership()).willReturn(resultC0);

        var result = controllerV1.getIdentityAttributesWithOwnership();

        assertThat(DtoUtils.jsonCompare(result, resultC0)).isTrue();
    }

    @Test
    void getParticipantIdentityAttributesTest() {
        var resultC0 = List.of(a(IdentityAttributeDTO.class));
        var certificateId = "junit-certificateId";
        var credentialId = "junit-credentialId";
        given(controller.getParticipantIdentityAttributes(credentialId)).willReturn(resultC0);

        var result = controllerV1.getParticipantIdentityAttributes(certificateId, credentialId);

        assertThat(DtoUtils.jsonCompare(result, resultC0)).isTrue();
    }

    @Test
    void pingAgentTest() {
        var fqdn = "junti-fqdn";
        var resultC0 = a(ParticipantWithIdentityAttributesDTO.class);
        given(controller.ping(fqdn)).willReturn(resultC0);
        var result = controllerV1.pingAgent(fqdn);
        assertThat(DtoUtils.jsonCompare(result, resultC0)).isTrue();
    }
}
