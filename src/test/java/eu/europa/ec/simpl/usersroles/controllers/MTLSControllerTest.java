package eu.europa.ec.simpl.usersroles.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.mockito.Mockito.verify;

import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.MtlsApi;
import eu.europa.ec.simpl.usersroles.mappers.ParticipantWithIdentityAttributesMapperImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({
    MTLSController.class,
    ParticipantWithIdentityAttributesMapperImpl.class,
})
class MTLSControllerTest {

    @MockitoBean(answers = Answers.RETURNS_DEEP_STUBS)
    MtlsApi mtlsApi;

    @Autowired
    MTLSController mtlsController;

    @Test
    void ping() {
        var credentialId = a(String.class);
        mtlsController.ping(credentialId);
        verify(mtlsApi).ping(credentialId);
    }
}
