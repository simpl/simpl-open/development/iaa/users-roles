package eu.europa.ec.simpl.usersroles.model.mappers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.usersroles.entities.IdentityAttributeWithOwnership;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeWithOwnershipMapper;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

class IdentityAttributeWithOwnershipMapperTest {
    private IdentityAttributeWithOwnershipMapper mapper;

    @BeforeEach
    void setup() {
        mapper = Mappers.getMapper(IdentityAttributeWithOwnershipMapper.class);
    }

    @Test
    void toEntityTest() {
        var entity = mapper.toEntity(a(IdentityAttributeWithOwnershipDTO.class));
        assertThat(entity).isNotNull();
        entity = mapper.toEntity(null);
        assertThat(entity).isNull();
    }

    @Test
    void toEntityTest_withNullValues() {
        var ia = a(IdentityAttributeWithOwnershipDTO.class);
        ia.setIdentityAttribute(null);
        ia.setAssignedToParticipant(null);
        var entity = mapper.toEntity(ia);
        assertThat(entity).isNotNull();
        entity = mapper.toEntity(null);
        assertThat(entity).isNull();
    }

    @Test
    void toLightDtoWithOwnership_successMapper() {
        var ia = a(IdentityAttributeWithOwnership.class);
        var entity = mapper.toLightDtoWithOwnership(ia);
        assertThat(entity).isNotNull();
    }

    @Test
    void toLightDtoWithOwnership_nullEntity() {
        var entity = mapper.toLightDtoWithOwnership(null);
        assertThat(entity).isNull();
    }

    @Test
    void toLightDto_successMapper() {
        var ia = a(IdentityAttributeWithOwnership.class);
        var entity = mapper.toLightDto(ia);
        assertThat(entity).isNotNull();
    }

    @Test
    void toLightDto_nullEntity() {
        var entity = mapper.toLightDto(null);
        assertThat(entity).isNull();
    }

    @Test
    void updateIdentityAttribute_successMapper() {
        var entity = new IdentityAttributeWithOwnership();
        var ida = a(IdentityAttributeDTO.class);
        assertDoesNotThrow(() -> mapper.updateIdentityAttribute(entity, ida));
    }

    @Test
    void updateIdentityAttribute_withNullValues() {
        var entity = new IdentityAttributeWithOwnership();

        var ida = a(IdentityAttributeDTO.class);
        ida.setAssignableToRoles(null);
        ida.setEnabled(null);
        entity.setParticipantTypes(null);
        assertDoesNotThrow(() -> mapper.updateIdentityAttribute(entity, ida));

        assertDoesNotThrow(() -> mapper.updateIdentityAttribute(new IdentityAttributeWithOwnership(), null));

        ida.setParticipantTypes(null);
        assertDoesNotThrow(() -> mapper.updateIdentityAttribute(new IdentityAttributeWithOwnership(), ida));

        var entityWithNotParticipantTypes = a(IdentityAttributeWithOwnership.class);
        entityWithNotParticipantTypes.setParticipantTypes(null);
        assertDoesNotThrow(() -> mapper.updateIdentityAttribute(entityWithNotParticipantTypes, ida));
    }

    @Test
    void toListDto_givenNullList_returnNullList() {
        var result = mapper.toListDto(null);
        assertThat(result).isNull();
    }

    @Test
    void toListDto_givenNotNullList_returnNotNullList() {
        List<eu.europa.ec.simpl.api.authenticationprovider.v1.model.IdentityAttributeWithOwnershipDTO> content =
                List.of();
        var result = mapper.toListDto(content);
        assertThat(result).isNotNull();
    }
}
