package eu.europa.ec.simpl.usersroles.configurations;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import eu.europa.ec.simpl.usersroles.configurations.keycloak.KeycloakConfig;
import eu.europa.ec.simpl.usersroles.configurations.keycloak.KeycloakProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({
    KeycloakConfig.class,
})
@EnableConfigurationProperties(KeycloakProperties.class)
@TestPropertySource(
        properties = {
            "keycloak.master.realm=realm",
            "keycloak.master.clientId=clientId",
            "keycloak.master.user=user",
            "keycloak.master.password=password",
            "keycloak.url=url",
        })
class KeycloakConfigTest {

    @Autowired
    private ApplicationContext context;

    @Test
    void loadContext() {
        assertNotNull(context);
    }
}
