package eu.europa.ec.simpl.usersroles.services.impl;

import static eu.europa.ec.simpl.common.test.TestUtil.*;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.*;

import eu.europa.ec.simpl.common.filters.KeycloakUserFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.usersroles.exceptions.KeycloakException;
import eu.europa.ec.simpl.usersroles.mappers.KeycloakMapperImpl;
import eu.europa.ec.simpl.usersroles.services.KeycloakService;
import jakarta.ws.rs.ClientErrorException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;
import org.instancio.Instancio;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RoleByIdResource;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

@ExtendWith(MockitoExtension.class)
class KeycloakUserServiceImplTest {

    @Spy
    KeycloakMapperImpl keycloakMapper;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    KeycloakService keycloakService;

    @InjectMocks
    KeycloakUserServiceImpl keycloakUserService;

    @Mock
    JwtService jwtService;

    @Test
    void createUser() throws URISyntaxException {

        // Given
        var expectedUserId = anUUID().toString();
        given(keycloakService.getAppUsers().create(any(UserRepresentation.class)))
                .willReturn(Response.created(new URI("http://authority/" + expectedUserId))
                        .build());
        // When
        var actualUserId = keycloakUserService.createUser(a(KeycloakUserDTO.class));
        // Then
        then(keycloakService.getAppUsers()).should().create(any(UserRepresentation.class));
        assertThat(actualUserId).isEqualTo(expectedUserId);
    }

    @Test
    void whenCreateUser_WhenKeycloakLocationIsInvalid_ShouldThrowIllegalStateException() throws URISyntaxException {

        // Given
        given(keycloakService.getAppUsers().create(any(UserRepresentation.class)))
                .willReturn(Response.created(new URI("malformedUrl")).build());

        // Then
        var ex = catchException(() -> keycloakUserService.createUser(a(KeycloakUserDTO.class)));

        assertThat(ex).as("check location exception").isNotNull().isInstanceOf(IllegalStateException.class);
    }

    @Test
    void createUserFail() {
        // Given
        var response = Mockito.mock(Response.class);
        given(response.getStatus()).willReturn(409);
        given(keycloakService.getAppUsers().create(any())).willReturn(response);
        // When
        var a = a(KeycloakUserDTO.class);
        var exception = catchThrowableOfType(() -> keycloakUserService.createUser(a), KeycloakException.class);
        // Then
        assertThat(exception.getStatus().isError()).isTrue();
    }

    @Test
    void getUserByEmail() {
        // Given
        var expectedUserEmail = Instancio.gen().net().email().get();
        var userRepresentation = Instancio.of(UserRepresentation.class).create();
        userRepresentation.setEmail(expectedUserEmail);
        var roleRepresentations = aListOf(RoleRepresentation.class);
        given(keycloakService
                        .getAppRealm()
                        .users()
                        .get(anyString())
                        .roles()
                        .realmLevel()
                        .listAll())
                .willReturn(roleRepresentations);
        given(keycloakService.getAppUsers().searchByEmail(eq(expectedUserEmail), any()))
                .willReturn(List.of(userRepresentation));
        given(keycloakMapper.toDto(any(), any()))
                .willReturn(Instancio.create(KeycloakUserDTO.class).setEmail(expectedUserEmail));

        // When
        var keycloakUserDTO = keycloakUserService.getUserByEmail(expectedUserEmail);
        // Then
        assertThat(keycloakUserDTO.getEmail()).isEqualTo(expectedUserEmail);
    }

    @Test
    void getUserByEmail_ClientThrowException_ExpectToMapException() {
        // Given
        var expectedUserEmail = Instancio.gen().net().email().get();
        var userRepresentation = Instancio.of(UserRepresentation.class).create();
        userRepresentation.setEmail(expectedUserEmail);

        var clientErrorException = generateClientErrorException();
        given(keycloakService.getAppUsers().searchByEmail(eq(expectedUserEmail), any()))
                .willThrow(clientErrorException);

        // When
        assertThrows(KeycloakException.class, () -> keycloakUserService.getUserByEmail(expectedUserEmail));
    }

    @Test
    void updateRole_Success() {
        var roleDTO = a(RoleDTO.class);
        var roleRep = a(RoleRepresentation.class);
        roleRep.setName(roleDTO.getName());

        var clientRoles = mock(RolesResource.class);
        var roleResource = mock(RoleResource.class);
        given(keycloakService.getAppRealm().roles()).willReturn(clientRoles);
        given(clientRoles.get(roleDTO.getName())).willReturn(roleResource);
        given(roleResource.toRepresentation()).willReturn(roleRep);

        keycloakUserService.updateRole(roleDTO);

        verify(clientRoles.get(roleDTO.getName()), times(1)).update(any(RoleRepresentation.class));
    }

    @Test
    void updateRole_KeycloakException() {
        var input = a(RoleDTO.class);
        doThrow(ClientErrorException.class).when(keycloakService).getAppRealm();
        assertThrows(ClientErrorException.class, () -> keycloakUserService.updateRole(input));
    }

    @Test
    void deleteRole_Success() {
        var input = "role-1";
        var roleRep = a(RoleRepresentation.class);
        roleRep.setName(input);
        var realmResource = mock(RealmResource.class);
        var rolesResources = mock(RolesResource.class);
        var roleResource = mock(RoleResource.class);
        given(keycloakService.getAppRealm()).willReturn(realmResource);
        given(realmResource.roles()).willReturn(rolesResources);
        given(rolesResources.get(input)).willReturn(roleResource);
        given(roleResource.toRepresentation()).willReturn(roleRep);

        keycloakUserService.deleteRole(input);

        verify(rolesResources, times(1)).deleteRole(input);
    }

    @Test
    void deleteRole_KeycloakException() {
        doThrow(ClientErrorException.class).when(keycloakService).getAppRealm();
        assertThrows(ClientErrorException.class, () -> keycloakUserService.deleteRole("role-1"));
    }

    @Test
    void importUsers() {
        // Given
        given(keycloakService.getAppUsers().create(any()))
                .willReturn(Response.created(anURI(anUUID().toString())).build());
        given(keycloakService.getClientRoles().get(any()).toRepresentation()).willReturn(a(RoleRepresentation.class));

        // When
        var users = aListOf(KeycloakUserDTO.class);
        keycloakUserService.importUsers(users);

        // Then
        then(keycloakService.getAppUsers()).should(times(users.size())).create(any());
    }

    @Test
    void importUser() {
        // Given
        given(keycloakService.getAppUsers().create(any()))
                .willReturn(Response.created(anURI(anUUID().toString())).build());
        given(keycloakService.getClientRoles().get(any()).toRepresentation()).willReturn(a(RoleRepresentation.class));

        // When
        keycloakUserService.importUser(a(KeycloakUserDTO.class));

        // Then
        then(keycloakService.getAppUsers()).should().create(any());
    }

    @Test
    void importRoles() {
        // When
        List<KeycloakRoleDTO> keycloakRoleDTOList =
                Instancio.ofList(KeycloakRoleDTO.class).create();
        keycloakUserService.importRoles(keycloakRoleDTOList);
        // Then
        int rolesCount = keycloakRoleDTOList.size();
        then(keycloakService.getAppRealm().roles()).should(times(rolesCount)).create(any());
    }

    @Test
    void importRole() {
        var role = mock(KeycloakRoleDTO.class);
        assertDoesNotThrow(() -> keycloakUserService.importRole(role));
    }

    @Test
    void getRole_whenUUID_thenReturnRoleRepresentation() {
        var id = UUID.randomUUID();
        var expected = an(RoleRepresentation.class);

        var realmResource = mock(RealmResource.class);
        var roleByIdResource = mock(RoleByIdResource.class);
        given(keycloakService.getAppRealm()).willReturn(realmResource);
        given(realmResource.rolesById()).willReturn(roleByIdResource);
        given(roleByIdResource.getRole(id.toString())).willReturn(expected);

        var result = keycloakUserService.getRole(id);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void getRole_whenUUID_thenThrowIllegalArgumentException() {
        var id = UUID.randomUUID();
        var exception = generateClientErrorException();
        given(keycloakService.getAppRealm()).willThrow(exception);

        assertThrows(KeycloakException.class, () -> keycloakUserService.getRole(id));
    }

    @Test
    void getRole_whenUUID_thenThrowNotFoundException() {
        var id = UUID.randomUUID();

        given(keycloakService.getAppRealm()).willThrow(NotFoundException.class);

        assertThrows(NotFoundException.class, () -> keycloakUserService.getRole(id));
    }

    @Test
    void getRole_throwClientError_throwKeycloakException() {
        var exception = generateClientErrorException();
        given(keycloakService.getAppRealm()).willThrow(exception);

        assertThrows(KeycloakException.class, () -> keycloakUserService.getRole("role-tests"));
    }

    @Test
    void isRolePresent_whenValidRoleName_whilReturnTrue() {
        var roleName = "Junit-rolename";
        var realmResource = mock(RealmResource.class);
        var rolesResource = mock(RolesResource.class);
        var roleResource = mock(RoleResource.class);
        given(rolesResource.get(roleName)).willReturn(roleResource);
        given(keycloakService.getAppRealm()).willReturn(realmResource);
        given(realmResource.roles()).willReturn(rolesResource);

        assertTrue(keycloakUserService.isRolePresent(roleName));
    }

    @Test
    void isRolePresent_whenNotFoundException_whillReturnFalse() {
        var roleName = "Junit-rolename";
        given(keycloakService.getAppRealm()).willThrow(NotFoundException.class);
        assertFalse(keycloakUserService.isRolePresent(roleName));
    }

    @Test
    void isRolePresent_whenClientErrroException_whilThrowKeycloakException() {
        var roleName = "Junit-rolename";
        var exception = generateClientErrorException();
        given(keycloakService.getAppRealm()).willThrow(exception);
        assertThrows(KeycloakException.class, () -> keycloakUserService.isRolePresent(roleName));
    }

    @Test
    void getUserRoles() {

        var uuid = UUID.randomUUID();
        var roleRepresentations = Instancio.ofList(RoleRepresentation.class).create();
        when(keycloakService
                        .getAppRealm()
                        .users()
                        .get(uuid.toString())
                        .roles()
                        .realmLevel()
                        .listAll())
                .thenReturn(roleRepresentations);
        // Then
        assertThat(keycloakUserService.getUserRoles(uuid.toString())).hasSize(roleRepresentations.size());
    }

    @Test
    void getUserRolesUserNotFound() {
        String uuid = "test-uuid";
        var realmResource = mock(RealmResource.class);
        var usersResource = mock(UsersResource.class);

        when(keycloakService.getAppRealm()).thenReturn(realmResource);
        when(realmResource.users()).thenReturn(usersResource);
        ClientErrorException clientErrorException = new ClientErrorException(Response.Status.BAD_REQUEST);
        when(usersResource.get(uuid)).thenThrow(clientErrorException);

        assertThrows(KeycloakException.class, () -> keycloakUserService.getUserRoles(uuid));
    }

    @Test
    void getUserByUuid() {
        var useruuid = UUID.randomUUID();
        var ur = an(UserRepresentation.class);

        var roleRepresentations = aListOf(RoleRepresentation.class);
        given(keycloakService
                        .getAppRealm()
                        .users()
                        .get(anyString())
                        .roles()
                        .realmLevel()
                        .listAll())
                .willReturn(roleRepresentations);
        given(keycloakService.getAppUsers().get(useruuid.toString()).toRepresentation())
                .willReturn(ur);
        given(keycloakMapper.toDto(any(), any()))
                .willReturn(Instancio.create(KeycloakUserDTO.class).setEmail(ur.getEmail()));
        KeycloakUserDTO keycloakUserDTO = keycloakUserService.getUserByUuid(useruuid.toString());
        assertThat(keycloakUserDTO.getEmail()).isEqualTo(ur.getEmail());
    }

    @Test
    void updateUserNotFound() {
        var useruuid = UUID.randomUUID();
        var userDTO = a(KeycloakUserDTO.class);

        var string = useruuid.toString();
        when(keycloakService.getAppUsers().get(string).toRepresentation())
                .thenThrow(new ClientErrorException(
                        Response.status(Response.Status.NOT_FOUND).build()));
        assertThatThrownBy(() -> keycloakUserService.updateUser(string, userDTO))
                .isInstanceOf(KeycloakException.class);
    }

    @Test
    void deleteUserNotFound() {
        UUID useruuid = UUID.randomUUID();

        var string = useruuid.toString();
        when(keycloakService.getAppUsers().delete(string)).thenThrow(ClientErrorException.class);
        assertThatThrownBy(() -> keycloakUserService.deleteUser(string)).isInstanceOf(ClientErrorException.class);
    }

    @Test
    void getClientRoles_Success() {
        var expectedRolesElement = a(RoleRepresentation.class);
        var expectedRoles = List.of(expectedRolesElement);
        when(keycloakService.getClientRoles().list()).thenReturn(expectedRoles);

        var result = keycloakUserService.getClientRoles();

        assertThat(result).isEqualTo(expectedRoles);
        verify(keycloakService.getClientRoles(), times(1)).list();
    }

    @Test
    void getClientRoles_ClientErrorException() {

        when(keycloakService.getClientRoles())
                .thenThrow(new ClientErrorException(
                        Response.status(Response.Status.NOT_FOUND).build()));

        assertThatExceptionOfType(KeycloakException.class)
                .isThrownBy(() -> keycloakUserService.getClientRoles())
                .satisfies(exception -> assertThat(exception.getStatus()).isEqualTo(HttpStatus.NOT_FOUND));
    }

    @Test
    void updateUserRoles_throwClientError_throwKeycloakException() {

        var ex = generateClientErrorException();
        given(keycloakService.getAppRealm()).willThrow(ex);
        var roles = List.of("TEST_ROLE");
        assertThrows(KeycloakException.class, () -> keycloakUserService.updateUserRoles("test-uuid", roles));
    }

    @Test
    void deleteUser_whenStatusIsNoConent_willNotThrowException() {
        var userId = UUID.randomUUID().toString();

        var usersResource = mock(UsersResource.class);
        var response = mock(Response.class);
        var status = Response.Status.NO_CONTENT.getStatusCode();
        given(response.getStatus()).willReturn(status);
        given(usersResource.delete(userId)).willReturn(response);
        given(keycloakService.getAppUsers()).willReturn(usersResource);

        assertDoesNotThrow(() -> keycloakUserService.deleteUser(userId));
    }

    @Test
    void deleteUser_whenStatusNotNoConent_willThrowKeycloakException() {
        var userId = UUID.randomUUID().toString();

        var usersResource = mock(UsersResource.class);
        var response = mock(Response.class);
        var status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
        given(response.getStatus()).willReturn(status);
        given(usersResource.delete(userId)).willReturn(response);
        given(keycloakService.getAppUsers()).willReturn(usersResource);

        assertThrows(KeycloakException.class, () -> keycloakUserService.deleteUser(userId));
    }

    @Test
    void search() {
        var userRepresentationsElement = an(UserRepresentation.class);
        var userRepresentations = List.of(userRepresentationsElement);
        var filter = a(KeycloakUserFilter.class);

        // Given
        var roleRepresentations = aListOf(2, RoleRepresentation.class);
        given(keycloakService
                        .getAppRealm()
                        .users()
                        .get(anyString())
                        .roles()
                        .realmLevel()
                        .listAll())
                .willReturn(roleRepresentations);
        given(keycloakService
                        .getAppUsers()
                        .search(
                                filter.getUsername(),
                                filter.getFirstName(),
                                filter.getLastName(),
                                filter.getEmail(),
                                filter.getFirst(),
                                filter.getMax(),
                                true,
                                false,
                                false))
                .willReturn(userRepresentations);
        // When
        var users = keycloakUserService.search(filter);

        // Then
        assertThat(users).hasSameSizeAs(userRepresentations);
    }

    @Test
    void getRoleList_success() {
        var realmResource = mock(RealmResource.class);
        var rolesResource = mock(RolesResource.class);
        var roleRepresentation = mock(RoleRepresentation.class);
        var roleRepresentations = List.of(roleRepresentation);

        when(keycloakService.getAppRealm()).thenReturn(realmResource);
        when(realmResource.roles()).thenReturn(rolesResource);
        when(rolesResource.list()).thenReturn(roleRepresentations);
        var result = keycloakUserService.getRoleList();

        assertThat(result).isEqualTo(roleRepresentations);
    }

    @Test
    void logout_success() {
        var claim = "sid";
        var sid = "junit-sid";
        var realmResource = mock(RealmResource.class);
        given(keycloakService.getAppRealm()).willReturn(realmResource);
        given(jwtService.getClaim(claim)).willReturn(sid);
        assertDoesNotThrow(() -> keycloakUserService.logout());
    }

    @Test
    void logout_willThrowClientException() {
        var exception = generateClientErrorException();
        var claim = "sid";
        var sid = "junit-sid";
        given(keycloakService.getAppRealm()).willThrow(exception);
        given(jwtService.getClaim(claim)).willReturn(sid);
        assertThrows(KeycloakException.class, () -> keycloakUserService.logout());
    }

    private ClientErrorException generateClientErrorException() {
        var clientErrorException = mock(ClientErrorException.class);
        var response = mock(Response.class);
        when(clientErrorException.getResponse()).thenReturn(response);
        when(response.getStatus()).thenReturn(500);
        return clientErrorException;
    }
}
