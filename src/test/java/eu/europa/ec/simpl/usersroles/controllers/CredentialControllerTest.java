package eu.europa.ec.simpl.usersroles.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.CredentialsApi;
import eu.europa.ec.simpl.api.authenticationprovider.v1.model.CredentialDTO;
import eu.europa.ec.simpl.usersroles.mappers.CredentialMapperImpl;
import eu.europa.ec.simpl.usersroles.mappers.ParticipantMapperImpl;
import eu.europa.ec.simpl.usersroles.services.CredentialService;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@ExtendWith(SpringExtension.class)
@Import({
    CredentialController.class,
    CredentialMapperImpl.class,
    ParticipantMapperImpl.class,
})
class CredentialControllerTest {

    @MockitoBean
    CredentialsApi credentialsApi;

    @MockitoBean
    CredentialService credentialService;

    @Autowired
    CredentialController credentialController;

    @Test
    void uploadCredential() {
        var mockMultipartFile = a(MockMultipartFile.class);
        credentialController.uploadCredential(mockMultipartFile);
        verify(credentialsApi).uploadCredential(mockMultipartFile);
    }

    @Test
    void hasCredentialShouldReturnTrueIfCredentialIsFound() {
        when(credentialsApi.getCredential()).thenReturn(a(CredentialDTO.class));
        var result = credentialController.hasCredential();
        verify(credentialsApi).getCredential();
        assertThat(result).isTrue();
    }

    @Test
    void hasCredentialShouldReturnFalseIfCredentialIsNotFound() {
        when(credentialsApi.getCredential()).thenThrow(HttpClientErrorException.NotFound.class);
        var result = credentialController.hasCredential();
        verify(credentialsApi).getCredential();
        assertThat(result).isFalse();
    }

    @Test
    void delete() {
        credentialController.delete();
        verify(credentialsApi).delete();
    }

    @Test
    void downloadInstalledCredentials() {
        var mockStreamingResponseBody = mock(StreamingResponseBody.class);
        when(credentialsApi.downloadInstalledCredentials()).thenReturn(mockStreamingResponseBody);
        var result = credentialController.downloadInstalledCredentials();
        assertThat(result).isEqualTo(mockStreamingResponseBody);
        verify(credentialsApi).downloadInstalledCredentials();
    }

    @Test
    void downloadLocalCredentials() throws IOException {
        var credential = "JUnit credential";
        when(credentialService.getCredential()).thenReturn(credential.getBytes());
        var result = credentialController.downloadLocalCredentials();
        var output = new ByteArrayOutputStream();
        result.writeTo(output);
        assertThat(output.toString()).isEqualTo(credential);
        verify(credentialService).getCredential();
    }

    @Test
    void getPublicKey() {
        credentialController.getPublicKey();
        verify(credentialsApi).getPublicKey();
    }

    @Test
    void getMyParticipantId() {
        credentialController.getMyParticipantId();
        verify(credentialsApi).getMyParticipantId();
    }
}
