package eu.europa.ec.simpl.usersroles.services.impl;

import static eu.europa.ec.simpl.common.test.TestUtil.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.usersroles.configurations.keycloak.KeycloakProperties;
import eu.europa.ec.simpl.usersroles.services.KeycloakService;
import eu.europa.ec.simpl.usersroles.services.KeycloakUserService;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ClientRoleToRealmRoleMapperTest {

    @Mock
    private KeycloakUserService keycloakUserService;

    @Mock
    private KeycloakService keycloakService;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private KeycloakProperties keycloakProperties;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private RealmResource realmResource;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ClientsResource clientsResource;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private UsersResource usersResource;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private RolesResource rolesResource;

    @InjectMocks
    private ClientRoleToRealmRoleMapper clientRoleToRealmRoleMapper;

    @Test
    void testInit() {
        when(keycloakService.getAppRealm()).thenReturn(realmResource);
        when(realmResource.clients()).thenReturn(clientsResource);
        when(realmResource.users()).thenReturn(usersResource);
        when(realmResource.roles()).thenReturn(rolesResource);
        var clientIds = List.of("client1");
        var clientRepresentation = a(ClientRepresentation.class);
        var roleRepresentation = a(RoleRepresentation.class);
        roleRepresentation.setName("role1");
        var roleRepresentationList = aListOf(1, RoleRepresentation.class);
        when(keycloakProperties.clientToRealmRoleMigration().clientIds()).thenReturn(clientIds);
        when(clientsResource.findByClientId(anyString())).thenReturn(List.of(clientRepresentation));
        when(clientsResource.get(anyString()).roles().list()).thenReturn(roleRepresentationList);
        when(keycloakUserService.isRolePresent(anyString())).thenReturn(false);
        doNothing().when(rolesResource).create(any(RoleRepresentation.class));
        when(usersResource.list()).thenReturn(List.of(a(UserRepresentation.class)));
        when(usersResource.get(anyString()).roles().clientLevel(anyString()).listAll())
                .thenReturn(roleRepresentationList);
        when(usersResource.get(anyString()).roles().realmLevel().listAll()).thenReturn(anEmptyList());
        when(rolesResource.get(anyString()).toRepresentation()).thenReturn(roleRepresentation);

        clientRoleToRealmRoleMapper.copy();

        verify(rolesResource, times(1)).create(any(RoleRepresentation.class));
        verify(usersResource.get(anyString()).roles().realmLevel(), times(1)).add(anyList());
    }
}
