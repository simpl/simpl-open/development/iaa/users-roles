package eu.europa.ec.simpl.usersroles.services.impl;

import static eu.europa.ec.simpl.common.test.TestUtil.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.filters.RoleFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.usersroles.entities.IdentityAttributeRoles;
import eu.europa.ec.simpl.usersroles.exceptions.*;
import eu.europa.ec.simpl.usersroles.mappers.RoleMapperImpl;
import eu.europa.ec.simpl.usersroles.repositories.IdentityAttributeRolesRepository;
import eu.europa.ec.simpl.usersroles.services.KeycloakUserService;
import eu.europa.ec.simpl.usersroles.services.RoleService;
import jakarta.ws.rs.ClientErrorException;
import jakarta.ws.rs.NotFoundException;
import java.util.*;
import org.instancio.Instancio;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.representations.idm.RoleRepresentation;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({RoleServiceImpl.class, RoleMapperImpl.class})
class RoleServiceImplTest {

    @MockBean
    private IdentityAttributeRolesRepository identityAttributeRolesRepository;

    @MockBean(answer = Answers.RETURNS_DEEP_STUBS)
    private KeycloakUserService keycloakService;

    @Autowired
    private RoleService roleService;

    @Test
    void testFindRoleByName_Success() {
        var roleId = UUID.randomUUID();
        var roleRepr = a(RoleRepresentation.class);
        roleRepr.setId(UUID.randomUUID().toString());
        var idaList = aListOf(IdentityAttributeRoles.class);

        given(keycloakService.getRole(roleId)).willReturn(roleRepr);
        given(identityAttributeRolesRepository.findByRoleNameAndEnabledTrue(anyString()))
                .willReturn(idaList);

        var output = roleService.findById(roleId);

        assertThat(output.getId()).isEqualTo(UUID.fromString(roleRepr.getId()));
        assertThat(output.getName()).isEqualTo(roleRepr.getName());
        assertThat(output.getDescription()).isEqualTo(roleRepr.getDescription());
        assertThat(output.getAssignedIdentityAttributes())
                .isEqualTo(
                        idaList.stream().map(IdentityAttributeRoles::getIdaCode).toList());
    }

    @Test
    void testFindRoleByName_NotFound() {
        var roleId = UUID.randomUUID();
        doThrow(NotFoundException.class).when(keycloakService).getRole(roleId);
        assertThrows(RoleNotFoundException.class, () -> roleService.findById(roleId));
    }

    @Test
    void testCreateRole_Success() {
        var input = a(KeycloakRoleDTO.class);
        var keycloakRole = a(RoleRepresentation.class);
        keycloakRole.setId(UUID.randomUUID().toString());

        given(keycloakService.isRolePresent(anyString())).willReturn(false);
        doNothing().when(keycloakService).importRole(any(KeycloakRoleDTO.class));
        given(keycloakService.getRole(anyString())).willReturn(keycloakRole);

        RoleDTO output = roleService.create(input);

        then(keycloakService).should(times(1)).importRole(any(KeycloakRoleDTO.class));
        assertThat(output.getId()).isEqualTo(UUID.fromString(keycloakRole.getId()));
        assertThat(output.getName()).isEqualTo(keycloakRole.getName());
        assertThat(output.getDescription()).isEqualTo(keycloakRole.getDescription());
    }

    @Test
    void testCreateRole_KeycloakError() {
        var input = a(KeycloakRoleDTO.class);
        given(keycloakService.isRolePresent(anyString())).willReturn(false);
        doThrow(ClientErrorException.class).when(keycloakService).importRole(any(KeycloakRoleDTO.class));
        assertThrows(KeycloakRoleException.class, () -> roleService.create(input));
    }

    @Test
    void testCreateRole_RoleAlreadySaved() {
        var input = a(KeycloakRoleDTO.class);
        given(keycloakService.isRolePresent(anyString())).willReturn(true);
        assertThrows(RoleDuplicationException.class, () -> roleService.create(input));
    }

    @Test
    void testUpdateRole_Success() {
        var roleId = UUID.randomUUID();
        var roleName = "role1";
        var input = a(RoleDTO.class).setId(roleId).setName(roleName);
        var found = a(RoleRepresentation.class);
        var idas = aListOf(IdentityAttributeRoles.class);
        found.setId(roleId.toString());
        found.setName(roleName);
        when(keycloakService.getRole(any(UUID.class))).thenReturn(found);
        when(identityAttributeRolesRepository.findByRoleNameAndEnabledTrue(anyString()))
                .thenReturn(idas);
        doNothing().when(keycloakService).updateRole(any(RoleDTO.class));

        var output = roleService.update(input);

        assertThat(output.getId()).hasToString(found.getId());
        assertThat(output.getName()).isEqualTo(found.getName());
        assertThat(output.getDescription()).isEqualTo(found.getDescription());
        assertThat(output.getAssignedIdentityAttributes())
                .isEqualTo(idas.stream().map(IdentityAttributeRoles::getIdaCode).toList());
    }

    @Test
    void testUpdateRole_NotFound() {
        var input = a(RoleDTO.class);
        when(keycloakService.getRole(any(UUID.class))).thenThrow(NotFoundException.class);
        assertThrows(RoleNotFoundException.class, () -> roleService.update(input));
    }

    @Test
    void testUpdateRole_NotMatchingName() {
        var roleId = UUID.randomUUID();
        var input = a(RoleDTO.class).setId(roleId);
        var found = a(RoleRepresentation.class);
        found.setId(roleId.toString());
        when(keycloakService.getRole(any(UUID.class))).thenReturn(found);
        when(identityAttributeRolesRepository.findByRoleNameAndEnabledTrue(anyString()))
                .thenReturn(Collections.emptyList());
        var exception = assertThrows(RoleUpdateException.class, () -> roleService.update(input));
        assertThat(exception.getMessage()).contains("field 'name' cannot be modified");
    }

    @Test
    void testUpdateRole_KeycloakError() {
        var roleId = UUID.randomUUID();
        var roleName = "role1";
        var input = a(RoleDTO.class).setId(roleId).setName(roleName);
        var found = a(RoleRepresentation.class);
        found.setId(roleId.toString());
        found.setName(roleName);
        var idas = aListOf(IdentityAttributeRoles.class);
        when(keycloakService.getRole(any(UUID.class))).thenReturn(found);
        when(identityAttributeRolesRepository.findByRoleNameAndEnabledTrue(anyString()))
                .thenReturn(idas);
        doThrow(ClientErrorException.class).when(keycloakService).updateRole(any(RoleDTO.class));

        assertThrows(KeycloakRoleException.class, () -> roleService.update(input));
    }

    @Test
    void testDeleteRole_Success() {
        var input = UUID.randomUUID();
        var roleRepr = a(RoleRepresentation.class);
        roleRepr.setId(UUID.randomUUID().toString());
        when(keycloakService.getRole(any(UUID.class))).thenReturn(roleRepr);
        doNothing().when(keycloakService).deleteRole(anyString());

        roleService.delete(input);

        then(identityAttributeRolesRepository).should(times(1)).disableMappingByRoleName(anyString());
        then(keycloakService).should(times(1)).deleteRole(anyString());
    }

    @Test
    void testDeleteRole_when_KeycloakException() {
        var input = UUID.randomUUID();
        var roleRepr = a(RoleRepresentation.class);
        roleRepr.setId(UUID.randomUUID().toString());
        when(keycloakService.getRole(any(UUID.class))).thenReturn(roleRepr);
        doThrow(NotFoundException.class).when(keycloakService).getRole(any(UUID.class));

        assertThrows(RoleNotFoundException.class, () -> roleService.delete(input));
    }

    @Test
    void testDeleteRole_KeycloakError() {
        var roleRepr = a(RoleRepresentation.class);
        roleRepr.setId(UUID.randomUUID().toString());
        when(keycloakService.getRole(any(UUID.class))).thenReturn(roleRepr);
        doThrow(ClientErrorException.class).when(keycloakService).deleteRole(anyString());

        assertThrows(KeycloakRoleException.class, () -> roleService.delete(UUID.randomUUID()));
    }

    @Test
    void testSearch_WithValidFilterAndPageable() {
        var roleRepr = a(RoleRepresentation.class);
        roleRepr.setId(UUID.randomUUID().toString());
        roleRepr.setName("Admin");
        roleRepr.setDescription("Admin role");
        Pageable pageable = PageRequest.of(0, 5);
        RoleFilter filter = new RoleFilter().setName("Admin");
        when(keycloakService.getRoleList()).thenReturn(List.of(roleRepr));

        var result = roleService.search(filter, pageable);

        assertThat(result).isNotNull().hasSize(1);
        assertThat(result.getContent().get(0).getName()).isEqualTo("Admin");
        verify(keycloakService, times(1)).getRoleList();
    }

    @Test
    void testSearch_WithEmptyResult() {
        Pageable pageable = PageRequest.of(0, 5);
        RoleFilter filter = new RoleFilter().setName("Admin");
        when(keycloakService.getRoleList()).thenReturn(anEmptyList());

        var result = roleService.search(filter, pageable);

        assertThat(result).isNotNull().isEmpty();
    }

    @Test
    void assignIdentityAttributes_replaceNewIdentityAttributesRoles_success() {
        var codes = List.of("ia1", "ia2", "ia3");
        var attributes = codes.stream()
                .map(code -> Instancio.create(IdentityAttributeRoles.class).setIdaCode(code))
                .toList();
        var roleRepr = a(RoleRepresentation.class);
        roleRepr.setId(UUID.randomUUID().toString());
        given(keycloakService.getRole(any(UUID.class))).willReturn(roleRepr);

        given(identityAttributeRolesRepository.saveAll(anyList())).willReturn(attributes);

        roleService.replaceIdentityAttributes(UUID.randomUUID(), codes);

        then(identityAttributeRolesRepository).should(times(1)).saveAll(anyList());
    }

    @Test
    void replaceIdentityAttributes_notExistRole_shouldThrowRoleNotFoundException() {
        var roleId = UUID.randomUUID();
        var ids = List.of("ia1", "ia2");

        given(keycloakService.getRole(roleId)).willThrow(NotFoundException.class);

        assertThrows(RoleNotFoundException.class, () -> roleService.replaceIdentityAttributes(roleId, ids));
    }

    @Test
    void removeAttributeRoleMapping() {
        var repres = a(RoleRepresentation.class);
        repres.setId(UUID.randomUUID().toString());
        given(keycloakService.getRole(any(UUID.class))).willReturn(repres);
        var attributeRoles = a(IdentityAttributeRoles.class);
        given(identityAttributeRolesRepository.deleteByRoleNameAndIdaCode(anyString(), anyString()))
                .willReturn(1L);

        roleService.removeAttributeForRole(attributeRoles.getIdaCode(), UUID.fromString(repres.getId()));

        then(identityAttributeRolesRepository)
                .should(times(1))
                .deleteByRoleNameAndIdaCode(repres.getName(), attributeRoles.getIdaCode());
    }

    @Test
    void removeAttributeRoleMappingNotFound() {
        var repres = a(RoleRepresentation.class);
        repres.setId(UUID.randomUUID().toString());
        given(keycloakService.getRole(any(UUID.class))).willReturn(repres);
        given(identityAttributeRolesRepository.findByRoleNameAndIdaCode(anyString(), anyString()))
                .willReturn(Optional.empty());
        roleService.removeAttributeForRole("ia1", UUID.randomUUID());
        then(identityAttributeRolesRepository).should(never()).delete(any(IdentityAttributeRoles.class));
    }

    @Test
    void duplicateIdentityAttributeToAnOtherRole_existingRoles_success() {
        var attributeRolesSourceList = aListOf(IdentityAttributeRoles.class);
        var attributeRolesDestinationList = attributeRolesSourceList.subList(0, 1);
        var roleReprSource = a(RoleRepresentation.class);
        roleReprSource.setId(UUID.randomUUID().toString());
        var roleReprDest = a(RoleRepresentation.class);
        roleReprDest.setId(UUID.randomUUID().toString());
        var sourceRoleId = UUID.fromString(roleReprSource.getId());
        var destRoleId = UUID.fromString(roleReprDest.getId());

        given(keycloakService.getRole(sourceRoleId)).willReturn(roleReprSource);
        given(identityAttributeRolesRepository.findByRoleNameAndEnabledTrue(roleReprSource.getName()))
                .willReturn(attributeRolesSourceList);
        given(keycloakService.getRole(destRoleId)).willReturn(roleReprDest);
        given(identityAttributeRolesRepository.findByRoleNameAndEnabledTrue(roleReprDest.getName()))
                .willReturn(attributeRolesDestinationList);

        roleService.duplicateIdentityAttributeToAnOtherRole(sourceRoleId, destRoleId);

        then(keycloakService).should(times(2)).getRole(any(UUID.class));
        then(identityAttributeRolesRepository).should(times(2)).findByRoleNameAndEnabledTrue(anyString());
        then(identityAttributeRolesRepository)
                .should(times(attributeRolesSourceList.size() - attributeRolesDestinationList.size()))
                .save(any(IdentityAttributeRoles.class));
    }

    @Test
    void duplicateIdentityAttributeToAnOtherRole_noExistingSourceRole_throwRoleNotFoundException() {
        var sourceRoleID = UUID.randomUUID();
        var destinationRoleId = UUID.randomUUID();

        given(keycloakService.getRole(any(UUID.class))).willThrow(NotFoundException.class);

        assertThrows(
                RoleNotFoundException.class,
                () -> roleService.duplicateIdentityAttributeToAnOtherRole(sourceRoleID, destinationRoleId));
    }

    @Test
    void duplicateIdentityAttributeToAnOtherRole_noExistingDestinationRole_throwRoleNotFoundException() {
        var sourceRoleId = UUID.randomUUID();
        var destinationRoleId = UUID.randomUUID();
        var attributeRolesList = aListOf(IdentityAttributeRoles.class);
        var roleRepr = a(RoleRepresentation.class);
        roleRepr.setId(UUID.randomUUID().toString());

        given(keycloakService.getRole(sourceRoleId)).willReturn(roleRepr);
        given(identityAttributeRolesRepository.findByRoleNameAndEnabledTrue(anyString()))
                .willReturn(attributeRolesList);
        given(keycloakService.getRole(destinationRoleId)).willThrow(NotFoundException.class);

        assertThrows(
                RoleNotFoundException.class,
                () -> roleService.duplicateIdentityAttributeToAnOtherRole(sourceRoleId, destinationRoleId));
    }

    @Test
    void findIdentityAttributesForUser() {
        var roleNames = List.of("ROLE1");
        var roleRepresentation = mock(RoleRepresentation.class);
        given(keycloakService.getRole(anyString())).willReturn(roleRepresentation);
        given(keycloakService.getRole(anyString()).getId())
                .willReturn(UUID.randomUUID().toString());
        roleService.findIdentityAttributesForUser(roleNames);
        then(identityAttributeRolesRepository).should(times(1)).findDistinctEnabledIdaCodesByRoleNames(anyList());
    }

    @Test
    void preAssignIdentityAttributesToRoleTest_success() {
        List<String> idaCodes = List.of("isaCode-junit");
        var roleName = "rolename-junit";

        roleService.preAssignIdentityAttributesToRole(idaCodes, roleName);

        verify(identityAttributeRolesRepository).saveAll(argThat(mappings -> {
            var role = (IdentityAttributeRoles) mappings.iterator().next();
            assertThat(role.getRoleName()).isEqualTo(roleName);
            assertThat(role.getIdaCode()).isEqualTo(idaCodes.getFirst());
            assertThat(role.getEnabled()).isFalse();
            return true;
        }));
    }
}
