package eu.europa.ec.simpl.usersroles.model.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import eu.europa.ec.simpl.api.authenticationprovider.v1.model.IdentityAttributeDTO;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeMapper;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

public class IdentityAttributeMapperTest {
    private IdentityAttributeMapper mapper;

    @BeforeEach
    void setup() {
        mapper = Mappers.getMapper(IdentityAttributeMapper.class);
    }

    @Test
    void toListDto_givenNullList_returnNullList() {
        var result = mapper.toListDto(null);
        assertThat(result).isNull();
    }

    @Test
    void toListDto_givenNotNullList_returnNotNullList() {
        List<IdentityAttributeDTO> list = List.of();
        var result = mapper.toListDto(list);
        assertThat(result).isNotNull();
    }
}
