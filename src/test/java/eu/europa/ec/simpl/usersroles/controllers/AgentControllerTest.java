package eu.europa.ec.simpl.usersroles.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static eu.europa.ec.simpl.common.test.TestUtil.anURI;
import static org.mockito.Mockito.verify;

import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.AgentsApi;
import eu.europa.ec.simpl.usersroles.mappers.EchoMapperImpl;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeMapperImpl;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeWithOwnershipMapperImpl;
import eu.europa.ec.simpl.usersroles.mappers.ParticipantWithIdentityAttributesMapperImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({
    AgentController.class,
    EchoMapperImpl.class,
    IdentityAttributeWithOwnershipMapperImpl.class,
    IdentityAttributeMapperImpl.class,
    ParticipantWithIdentityAttributesMapperImpl.class,
})
class AgentControllerTest {

    @MockitoBean
    AgentsApi agentsApi;

    @Autowired
    AgentController agentController;

    @Test
    void echo() {
        agentController.echo();
        verify(agentsApi).echo();
    }

    @Test
    void getIdentityAttributesWithOwnership() {
        agentController.getIdentityAttributesWithOwnership();
        verify(agentsApi).getIdentityAttributesWithOwnership();
    }

    @Test
    void getParticipantIdentityAttributes() {
        var certificateId = a(String.class);
        agentController.getParticipantIdentityAttributes(certificateId);
        verify(agentsApi).getParticipantIdentityAttributes(certificateId);
    }

    @Test
    void ping() {
        String fqdn = anURI().getHost();
        agentController.ping(fqdn);
        verify(agentsApi).pingAgent(fqdn);
    }
}
