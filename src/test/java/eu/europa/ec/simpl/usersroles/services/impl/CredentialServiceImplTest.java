package eu.europa.ec.simpl.usersroles.services.impl;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;
import static org.mockito.BDDMockito.given;

import eu.europa.ec.simpl.usersroles.entities.Credential;
import eu.europa.ec.simpl.usersroles.exceptions.CredentialsNotFoundException;
import eu.europa.ec.simpl.usersroles.repositories.CredentialRepository;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CredentialServiceImplTest {

    @Mock
    CredentialRepository repository;

    @InjectMocks
    CredentialServiceImpl credentialService;

    @Test
    void getCredential_whenCredentialIsPresent_willReturnTheCredential() {
        var expectedCredential = a(Credential.class);
        // Given
        given(repository.findAll()).willReturn(List.of(expectedCredential));
        // When
        var actualCredential = credentialService.getCredential();
        // Then
        assertThat(actualCredential).isEqualTo(expectedCredential.getContent());
    }

    @Test
    void getCredential_whenCredentialIsPresent_willThrowCredentialsNotFoundException() {
        // Given
        given(repository.findAll()).willReturn(List.of());
        // When
        var exception = catchException(() -> credentialService.getCredential());
        // Then
        assertThat(exception).isInstanceOf(CredentialsNotFoundException.class);
    }
}
