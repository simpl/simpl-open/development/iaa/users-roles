package eu.europa.ec.simpl.usersroles.model.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import eu.europa.ec.simpl.usersroles.entities.IdentityAttributeRoles;
import eu.europa.ec.simpl.usersroles.mappers.RoleMapper;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

public class RoleMapperTest {
    private RoleMapper mapper;

    @BeforeEach
    void setup() {
        mapper = Mappers.getMapper(RoleMapper.class);
    }

    @Test
    void roleIdaToRoleDTOIda_givenNullList_returnNullList() {
        var result = mapper.roleIdaToRoleDTOIda(null);
        assertThat(result).isNull();
    }

    @Test
    void roleIdaToRoleDTOIda_givenNotNullList_returnNotNullList() {
        List<IdentityAttributeRoles> rolesIda = List.of();
        var result = mapper.roleIdaToRoleDTOIda(rolesIda);
        assertThat(result).isNotNull();
    }
}
