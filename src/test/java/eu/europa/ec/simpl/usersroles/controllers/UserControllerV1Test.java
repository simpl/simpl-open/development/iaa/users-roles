package eu.europa.ec.simpl.usersroles.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import eu.europa.ec.simpl.api.usersroles.v1.model.KeycloakUserDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.usersroles.mappers.UserMapperV1Impl;
import eu.europa.ec.simpl.usersroles.utils.DtoUtils;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({
    UserMapperV1Impl.class,
    UserControllerV1.class,
})
public class UserControllerV1Test {

    private static UUID randomUUID = UUID.fromString("914c1082-b495-418a-8064-028b6728cf17");

    @MockitoBean
    private UserController controller;

    @Autowired
    private UserControllerV1 controllerV1;

    @Test
    void createUserTest() {
        var keycloakUserDTO = a(KeycloakUserDTO.class);
        var resultC0 = "junit-result-controller0";
        given(controller.createUser(argThat(dto -> DtoUtils.jsonCompare(dto, keycloakUserDTO))))
                .willReturn(resultC0);
        var result = controllerV1.createUser(keycloakUserDTO);
        assertThat(result).isEqualTo(resultC0);
    }

    @Test
    void createUserAsT1Test() {
        var keycloakUserDTO = a(KeycloakUserDTO.class);
        var resultC0 = "junit-result-controller0";
        given(controller.createUserAsT1(argThat(dto -> DtoUtils.jsonCompare(dto, keycloakUserDTO))))
                .willReturn(resultC0);
        var result = controllerV1.createUserAsT1(keycloakUserDTO);
        assertThat(result).isEqualTo(resultC0);
    }

    @Test
    void deleteUserTest() {
        var uuid = randomUUID.toString();
        controllerV1.deleteUser(uuid);
        verify(controller).deleteUser(eq(uuid));
    }

    @Test
    void getRolesTest() {
        var uuid = randomUUID.toString();
        var rolesC0 = List.of(a(KeycloakRoleDTO.class));
        given(controller.getRoles(uuid)).willReturn(rolesC0);
        var result = controllerV1.getRoles(uuid);
        assertThat(DtoUtils.jsonCompare(result, rolesC0)).isTrue();
    }

    @Test
    void getUserByUuidTest() {
        var uuid = randomUUID.toString();
        var userC0 = a(eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO.class);
        given(controller.getUserByUuid(uuid)).willReturn(userC0);
        var result = controllerV1.getUserByUuid(uuid);
        assertThat(DtoUtils.jsonCompare(result, userC0)).isTrue();
    }

    @Test
    void importUsersTest() {
        var requestBody = List.of(a(KeycloakUserDTO.class));
        controllerV1.importUsers(requestBody);
        verify(controller).importUsers(argThat(dto -> DtoUtils.jsonCompare(dto, requestBody)));
    }

    @Test
    void searchTest() {
        String username = "junit-username";
        String firstName = "junit-firstName";
        String lastName = "junit-lastName";
        String email = "junit-email";
        Integer first = 3;
        Integer max = 100;
        var keycloakUser = a(eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO.class);
        var resultC0 = List.of(keycloakUser);
        given(controller.search(argThat(filters -> {
                    return filters.getUsername().equals(username)
                            && filters.getFirstName().equals(firstName)
                            && filters.getLastName().equals(lastName)
                            && filters.getEmail().equals(email)
                            && filters.getFirst().equals(first)
                            && filters.getMax().equals(max);
                })))
                .willReturn(resultC0);

        var result = controllerV1.search(username, firstName, lastName, email, first, max);

        assertThat(DtoUtils.jsonCompare(result, resultC0)).isTrue();
    }

    @Test
    void updateUserTest() {
        var uuid = randomUUID.toString();
        var keycloakUserDTO = a(KeycloakUserDTO.class);
        controllerV1.updateUser(uuid, keycloakUserDTO);
        verify(controller).updateUser(eq(uuid), argThat(dto -> DtoUtils.jsonCompare(dto, keycloakUserDTO)));
    }

    @Test
    void updateUserRolesTest() {
        var uuid = randomUUID.toString();
        var requestBody = List.of("junit-value-1", "junit-value-1");
        controllerV1.updateUserRoles(uuid, requestBody);
        verify(controller).updateUserRoles(uuid, requestBody);
    }
}
