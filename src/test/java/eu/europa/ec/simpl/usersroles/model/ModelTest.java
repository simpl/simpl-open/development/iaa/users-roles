package eu.europa.ec.simpl.usersroles.model;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.test.TestUtil;
import eu.europa.ec.simpl.usersroles.entities.IdentityAttributeWithOwnership;
import java.lang.reflect.Method;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ModelTest {

    public static Stream<Arguments> dtoTest() {
        return Stream.of(arguments(KeycloakRoleDTO.class), arguments(IdentityAttributeWithOwnership.class));
    }

    @MethodSource
    @ParameterizedTest
    <T> void dtoTest(Class<T> type) {
        var obj = TestUtil.a(type);
        assertNotNull(obj);
        exploreGetter(obj);
    }

    public void exploreGetter(Object obj) {
        for (Method m : obj.getClass().getMethods()) {
            if ((m.getName().startsWith("get") || m.getName().startsWith("is")) && m.getParameterCount() == 0) {
                try {
                    m.invoke(obj);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
